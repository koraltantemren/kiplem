module.exports = (dataKiplem) => {
  let typeCheck = (rule, input) => {
    if(rule instanceof Array){
      return rule.indexOf(input)!=-1||(isFinite(input)&&rule.indexOf(Number(input))!=-1);
    }

    if(typeof rule === 'function'){
      return rule(input) || false
    }

    if(rule instanceof Object){
      if(rule.length){
        let u=String(input).length
        if(u < rule.length[0] || (rule.length[1] ? u > rule.length[1] : false) || (((!(rule.length instanceof Array)) ? u>=rule.length : false))){
          return false
        }
      }


      if(isFinite(input) && rule.range){
        let u=Number(input)
        if(u < rule.range[0] || (rule.range[1] ? u > rule.range[1] : false) || (!(rule instanceof Array) && u>=rule.range)){
          return false
        }
      }

      if(rule['regex'] && !input.match(rule['regex'])){
        return false
      }

      if(rule['replace'] && !(input = input.replace(rule['replace'][0], rule['replace'][1]))){
        return false
      }

      if(rule.type){
        switch(rule.type){
          case String:
            return !(typeof input !='string'||(rule.set&&rule.set.indexOf(input)==-1))?input:false
          break;
          case Number:
            return isFinite(input)?Number(input):false
          break;
          case Array:
            if(rule.objects){
              let _try, exception = input.some((nesne, s) => {
                _try = typeCheck(rule.objects,nesne)
                return _try?((input[s]=_try) && false):true
              })

              return exception?false:input
            }
          break;
          default:
            if(!(input instanceof rule.type)){
              return false
            }
          break;
        }
      }

      if(rule['_']){
        let audit = check(rule['_'], input)
        input = audit ? audit : false
      }
    }

    return input
  }

  let check = (kiplem, data) => {
    let rules = kiplem.grk || kiplem.ist ? {...kiplem.grk, ...kiplem.ist} : kiplem,
      error = 1, trial
    if(data instanceof Array){
      data = Object.assign(...(Object.keys(rules)).map((a,i) => ({[a]: data[i]})))
    }
    let keys = Object.keys(data);
    if(!keys.length){return false;}
    if((!dataKiplem.grk || !Object.keys(dataKiplem.grk).filter(b => {keys.indexOf(b)==-1}).length)
    ){
      let rule,_data
      error = keys.some((a) => {
        rule = rules[a] ? rules[a] : rules['_']
        _data = rules[a] ? data[a] : data
        if(!rule){return dataKiplem.ist && (dataKiplem.ist === '_' || dataKiplem.ist['_']) ? false : true}
        trial = typeCheck(rule, _data)
        return trial ? ((rules[a] ? (data[a]=trial) : (data=trial)) && false) : true
      })
    }
    if(kiplem['_']){
      trial = typeCheck(rules['_'], data)
      if(trial){data=trial}else{error=1}
    }

    return error?false:data
  }

  return (i,y,next) => {
    let error = false, data=i.body
    if(i.method == 'POST'){
      let audit = check(dataKiplem, data)
      if(audit){
        i.body=audit
        next()
        return;
      }
    }
    y.status(402).end()
  }
}
